# Contributing to Apptainer images of Aiida

This documentation provides everything you need to contribute to the Apptainer Aiida image.

## Technical aspects

To develop new features for the Apptainer images of Aiida, you need to clone this repository and to create a new branch:
```bash
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:almeidjp/aiida2dahu.git
git switch -C new_branch
```

Once this is done, you can start working on the new feature. There are several ways to contribute to this project:
- add/edit a computer (via a pair of a setup and a config)
- add/edit a code
- add/edit a ssh config
- add/edit a tutorial
- add/edit a daughter Apptainer image of Aiida

> I **strongly** recommend adding at least one paragraph of documentation to help users with your new implementations. You can either edit the files in the `./tutorials/` folders or create a new one!

Once you are happy with your implementation, simply create a **merge request** and specify "Benjamin Arrondeau" (@arrondeb) as `Assignee` and `Reviewer`.

### Adding a new computer

Adding a new computer configuration simplifies the user experience and reduces the problems you may encounter with a tricky installation. To add a new computer configuration file, you should place both the `setup` and the `config` files in `./yml_files/computers/`. I recommend starting with the `setup` file and looking at the configurations that are already in the repository.

### Adding a new code

Adding a new code configuration simplifies the user experience and provides the user with the exact command to use to run your code. To add a new code configuration file, you should provide the yml files in `./yml_files/codes/`. I recommend looking at the configurations that are already in the repository.

### Adding a new ssh config

Adding a new ssh config simplifies the user experience and helps the user if your code requires ssh connectivity to e.g. HPC infrastructures. To add a new ssh config file, you should make the `config` files available in `./ssh/`. I recommend looking at the configurations that are already in the repository.

### Adding a new environment variable

Adding a new environment variable simplifies the user experience and provides a single file to edit all existing configuration files. To add a new environment variable, you should add it to the `config.json` files in `./bash_script/`. First of all, I recommend to find out what kind of variable can be useful for ease of use. Then, you should use an **explicit** name (like `perseus-login`). To refer to this variable in the yml files or the ssh config files, you need write it as an environment variable (like `$perseus-login`).

### Adding a new daughter Apptainer image of Aiida

The way the repository works is based on core/daughter images. Briefly, the `aiida_core` image contains all the necessary plugins and configuration files. Then, the `aiida_core` image is enriched with specific plugins, data or workflows. It is then considered as a daughter image. Adding a new daughter image allows the user to have all the requirements to run your workflow. To add a new daughter image, you should provide the `daughter_image.def` files in `./apptainer/`. I recommend looking at the `./apptainer/aiida_vasp.def` file that is already in the repository.


You can take a look at the `aiida_vasp` image. 

## Testing your new implementations on your local machine

The first thing to do, is to install [Apptainer](https://diamond-diadem.github.io/fr/documentation/install-apptainer/howto/). Once this is done, open a terminal and go to the `apptainer` folder.

You can only test your new implementations with a daughter image. This means, you need to build the core image and one daughter image of your choice on your local machine. The first thing you need to do is to modify the *definition file* of the Aiida's core image. To do this, you need to uncomment the line `# git switch <new_branch>` and replace `<new_branch>` with the name of your branch. This will force the use of your git branch with the implementations you have developed. Once this is done, you can run the following commands:
```bash
# to build the core Apptainer image of Aiida 
apptainer build aiida_core.sif aiida_core.def

# to build the daughter Apptainer image of Aiida
apptainer build <daughter_image>.sif <daughter_image>.def
```

Once the images are built, I recommend following the [`use_apptainer` tutorial](./tutorials/use_apptainer.md) to run an instance, access the container via `shell` and install your new implementation (ssh config, computer or code).