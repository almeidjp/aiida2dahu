## Using sshpass to access a HPC server that requires password:

First step necessary to do so is to install `sshpass` in your local machine:

    sudo apt install sshpass

 Then, you will need to register your password to access the machine somewhere. It can be passed directly in the command, but this is not advisable for safety reasons. A reasonably more safe option is to store it in a file hidden file and change this file permissions to make it harder to be hacked. 

    echo 'MyP4ssw0rd!' > /adress/.file
    sudo chmod 400 /adress/.file

To use access the server, you can do it directly in the terminal using `sshpass` via:

    sshpass -f /adress/.file ssh myusername@remote.hcp.ip

A smart alternative is to convert `ssh` in a function in your `.bashrc`. This works like an alias, allowing you to use the traditional `ssh myusername@remote.hcp.ip` and benefit of not using the password directly. To do so, append the following lines to your `.bashrc`, doing the necessary adaptations:

```bash
    ssh() {
        if [ "$1" = "myusername@remote.hcp.ip" ]; then
            shift
            sshpass  -f /adress/.file ssh myusername@remote.hcp.ip "$@"
        else 
            command ssh "$@"
        fi
    }
```

If you need to list many servers, additional `elif [ "$1" = "myusername@other.hcp.ip" ]; then` can be added following the bash else-if syntax. 
