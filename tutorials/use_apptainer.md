# How to use Apptainer image of Aiida

## Step 0 : Before using the image

**If you have never used Apptainer before**, we recommand that you to install it on your local machine by following these [steps](https://diamond-diadem.github.io/en/documentation/install-apptainer/howto/) or check the documentation of the HPC cluster you want to use to see if it is available.

In order to use the Apptainer image of Aiida, you need a specific folder architecture. To make a long story short, we recommend that you use this folder architecture:

```bash
$ ls /path/to/folder/of/your/choice
└── .aiida (final environment of the container)
    ├── database
    ├── .ssh
    ├── postgres_run
    └── rabbitmq
         └── var
             ├── lib/rabbitmq
             └── log
```

Here is the long story:
* The `database` folder is needed to store the data related to Aiida
* The `.ssh` folder is needed to store the data related to ssh connections
* The `postgres_run` folder is needed to run the PostgreSQL service
* The `rabbitmq/var/lib/rabbitmq` and `rabbitmq/var/log` folders are required for that the RabbitMQ service to run

Once you have created the folder architecture and you have access to Apptainer, you are ready to use the Apptainer image of Aiida.

## Step 1 : Download the image you need

The first thing you need to do if you want to download any registry images is to get a token. This is done with the following command:

```bash
apptainer remote login --username $gitlab_username --password $gitlab_password oras://gricad-registry.univ-grenoble-alpes.fr/almeidjp/aiida2dahu
```

The output should be `INFO:    Token stored in $HOME/.apptainer/remote.yaml`. Then, you need to go to the `Deploy` section on the left panel and the `Container Registry` section. Select the image you are interested in (e.g., `aiida_vasp`) and download it using the command:

```bash
apptainer pull oras://gricad-registry.univ-grenoble-alpes.fr/almeidjp/aiida2dahu/aiida_vasp:latest
```

> **Caution!** In order to use this command, you need at least Apptainer 

## Step 2 : Launching an instance 

Once you have downloaded the image you want, it is time to use it! We recommend that you start an instance first, and then access the container via the shell command, or run the commands you want. To start an instance, you should run the following code:

```bash
apptainer instance start \
        --containall \
        -B $PATH/.aiida:/.aiida \
        -B $PATH/.aiida/.ssh:$HOME/.ssh \
        -B $PATH/.aiida/postgres_run:/var/run/postgresql \
        -B $PATH/.aiida/rabbitmq/var/lib/rabbitmq:/var/lib/rabbitmq \
        -B $PATH/.aiida/rabbitmq/var/log:/var/log/rabbitmq \
        apptainer_image_name.sif instance_name
```

Here, `$PATH` is an environment variable pointing to `/path/to/folder/of/your/choice` and the `-B` flags allow specific folders to be mounted on the Apptainer image. Once the instance is started, you can either access to the container using `apptainer shell instance://aiida_core` or run commands using `apptainer exec instance://aiida_core commands ...`. However, **before doing anything** with the Apptainer image, you should configure your Aiida profile.

## Step 3 : Accessing the container

To access in the container you have just started via `apptainer instance start`, simply run the following command:

```bash
apptainer shell instance://instance_name
```

> The first command you should run when you enter the container is `source /root/envs/aiida/bin/activate`. This will activate the Aiida environment and allow you to run `verdi` commands.

## Step 4 : Setting up the Aiida environment

### Setting up a user profile

A default user profile is available in the container: `aiida_user`. However, you can create your own user profile using the following commands:

```bash
verdi quicksetup --profile your_profile --email your@mail --first-name your_firstname --last-name your_lastname --institution your_institution
verdi profile setdefault your_profile
verdi config set warnings.rabbitmq_version false
```

> The last command disables RabbitMQ warnings for your_profile.

### Setting up the aiida environment

Before configuring any computers or codes, we recommend that you set up your aiida environment. To do this, you can configure some environment variables in a `config.json` file using the following command:

```bash
nano-tiny /tmp/config.json
```

Then, you can run the command `bash /tmp/config_env.sh` to fully configure your aiida environment. This script will customise the default configuration files with the information you provide, making it easier to set up codes, computers and ssh connections.

### Setting up a seamless ssh connection to Gricad clusters from an Apptainer image

This section is intended for people who already have a PERSEUS account and are assigned to a project. If this is not your case, please check this [link](https://gricad-doc.univ-grenoble-alpes.fr/services/)

The first thing you need to do is to generate an RSA key. To do this, issue the following command: 
```bash
ssh-keygen
```

> It is very important to leave the password empty so that Aiida can access the Gricad facilities without any problems.

Then, you need to set up a transparent SSH connection. To do this, copy the `/tmp/workspace/ssh/config` file to `~/.ssh/`. Finally, you can add your RSA key to the head nodes and the clusters using the following commands:

```bash
ssh-copy-id <perseus-login>@rotule.univ-grenoble-alpes.fr
ssh-copy-id <perseus-login>@trinity.univ-grenoble-alpes.fr
ssh-copy-id dahu.ciment
```

You can try to access the `dahu` cluster (with `ssh dahu.ciment`) to test if everything went well.

### Setting up a computer

You can either take a look to some of the default computers in the `/tmp/workspace/yml_files/computers/setup` folder or write a custom `yml` file. To set up a new computer, you need to run the following commands:

```bash
verdi computer setup -n --config /tmp/workspace/yml_files/computers/setup/computer.yml
verdi computer configure <transport> -n --config /tmp/workspace/yml_files/computers/config/computer.yml <computer_name>
```

> Here, you need to change <transport> and <computer_name> to the values you have chosen

You can check if the installation went well with the `verdi computer list` command.

### Setting up a code

You can either have a look at some default codes in the folder `/tmp/workspace/yml_files/codes/` or write a custom `yml` file. To create a new code, you need to run the following command:

```bash
verdi code create core.code.installed -n --config /tmp/workspace/yml_files/codes/code.yml
```

You can check if the installation went well with the `verdi code list` command.

## Step 5 : Running a workflow

Once you have set up your environment correctly, you are ready to use the image and setup workflows. You can use either the `bash` or `exec` commands. We recommend that you use the `shell` command to access the container. We also recommend moving the workflow python file to `/tmp`.

To run a workflow, all you need to do is use the command `verdi run path/to/your/workflow/run.py`.

> **Caution!** Before running a workflow, you should look carefully at the python file and set up the correct <code_name> and <username>.

## Step 6 : Stopping an instance 

Once you have finished using the apptainer image, simply run the following command to stop the instance:

```bash
apptainer instance stop instance_name
```

## Additional steps

### Adding data to a code

Some codes and workflows require data. For example, VASP needs potentials to run correctly. In fact, you need to register data (as you did for the computer and the code). However, the way Aiida registers data is by providing a `.tar` or `.tar.gz` file and unpacking it. This means that you need to mount the `/tmp` folder on the container when you are configuring it. In practice, you need to add this line `-B /tmp:/tmp \` when starting the instance. Then, you can use the following command:

```bash
verdi data vasp-potcar uploadfamily --path=/tmp/archive --name=PBE.54 --description="PBE potentials version 54"
```

To check that the installation is correct, you can run `verdi data vasp-potcar listfamilies`. The output of this command should **not** be empty.

> Once you have added the data, we recommend that you to stop the instance and start a new one without the `/tmp` folder mounted on the container.

### Workflow on Dahu

You need to specify a Perseus project if you want to run workflows on the Dahu cluster from Gricad. To do this, you can write it as your username in your workflow python file.
