# AiiDA2dahu

Package with all produced plugins, containers and documentation necessary to run AiiDA direcly in DAHU machine dedicated to workflows.

## File structure

- __aiida2dahu\-main__
   - [README.md](README.md)
   - __aiida\-diamond__  *(Plugin AiiDA to use GRICAD and TGCC machines)*
     - [LICENSE](aiida-diamond/LICENSE)
     - [README.md](aiida-diamond/README.md)
     - __aiida\_diamond__
       - __schedulers__
         - [dahu\_scheduler.py](aiida-diamond/aiida_diamond/schedulers/dahu_scheduler.py)
         - [tgcc\_scheduler.py](aiida-diamond/aiida_diamond/schedulers/tgcc_scheduler.py)
     - [pyproject.toml](aiida-diamond/pyproject.toml)
   - __bash\_script__ *(Scripts to configure and launch a container with AiiDA)*
     - [check\_aiida\_user.sh](bash_script/check_aiida_user.sh)
     - [config.json](bash_script/config.json)
     - [config\_env.sh](bash_script/config_env.sh)
     - [instance\_start.sh](bash_script/instance_start.sh)
     - [runtime.sh](bash_script/runtime.sh)
     - [state.env](bash_script/state.env)
   - __ssh__ *(ssh config for transparent ssh proxy)*
     - [config](ssh/config)
   - __tutorials__ *(Extra documantation to help configure and use AiiDA)*
     - [connect\_irene.md](tutorials/connect_irene.md)
     - [use\_apptainer.md](tutorials/use_apptainer.md)
   - __yml\_files__ *(AiiDA YAML config files for use with `verdi code/computer`)*
     - __codes__
       - [vasp\_gricad\_dahu.yml](yml_files/codes/vasp_gricad_dahu.yml)
     - __computers__
       - __config__
         - [gricad\_dahu.yml](yml_files/computers/config/gricad_dahu.yml)
         - [local\_computer.yml](yml_files/computers/config/local_computer.yml)
       - __setup__
         - [gricad\_dahu.yml](yml_files/computers/setup/gricad_dahu.yml)
         - [local\_computer.yml](yml_files/computers/setup/local_computer.yml)


## Functionalities of the images

For now, here are the default functionalities of the different images:

- __computers__:
  - Dahu cluster from Gricad
  - Local computer
- __codes__:
  - VASP (to run on Dahu cluster)
- __ssh config__:
  - Config for Dahu cluster

## List of available images

For now, here are the daughter Aiida's apptainer images available on the [`Container Registry`](https://gricad-gitlab.univ-grenoble-alpes.fr/almeidjp/aiida2dahu/container_registry/):

- `aiida_vasp`

## Use an Aiida's Apptainer image

If you want to use an Aiida's Apptainer image, please read this [file](./tutorials/use_apptainer.md).

## Contributing

If you want to contribute to this projects, please read this [file](./CONTRIBUTING.md).