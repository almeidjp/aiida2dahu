#! /bin/bash

cp -r /workspace /tmp
declare -A array

for i in $( cat /tmp/config.json | grep : | awk '{ print $1 "°" $3 }' )
   do
      IFS="°," read -r var1 var2 <<< $i
      var1="${var1//\"/}"
      var2="${var2//\"/}"
      array[$var1]=$var2
   done

for file in "$( find /tmp/workspace/yml_files/ -type f -follow -print )"
   do
      for key in "${!array[@]}"
         do
           sed -i 's@$'"$key"'@'"${array[$key]}"'@g' $file
         done
   done

for file in "$( find /tmp/workspace/ssh/ -type f -follow -print )"
   do
      for key in "${!array[@]}"
         do
           sed -i 's@$'"$key"'@'"${array[$key]}"'@g' $file
         done
   done
