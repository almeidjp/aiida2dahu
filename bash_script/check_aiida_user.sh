# We activate the aiida virtual env
source /root/envs/aiida/bin/activate

# We activate the aiida virtual env
it_profile=$(verdi profile list | grep -v "Report" | grep -v "Warning" | wc -l)
if [ ${it_profile} -eq 0 ]; then
	verdi quicksetup --profile aiida_user --email aiida@user.com --first-name Aiida --last-name User --institution apptainer
	verdi profile setdefault aiida_user
	verdi config set warnings.rabbitmq_version false
fi

# We deactivate the aiida virtual env
deactivate
